<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($_POST["type"] == "addOrEdit") {
        $group = $_POST["group"];
        $firstName = $_POST["firstName"];
        $lastName = $_POST["lastName"];

        $gender = $_POST["gender"];
        $birthday = $_POST["birthday"];

        $errors = new stdClass();
            if (empty($group)) {
                $errors -> errorText = "Empty group";
            }


            if (empty($firstName)) {
                $errors -> errorText = "Empty first name";
            }
            if(empty($lastName)){
                $errors -> errorText = "Empty last name";
            }


            if (empty($gender)) {
                $errors -> errorText = "Empty gender";
            } elseif($gender != "1" && $gender != "2" && $gender != "3") {
                $errors -> errorText = "Wrong gender value";
            }


            if (empty($birthday)) {
                $errors -> errorText = "Empty birthday";
            }


            $response = new stdClass();
            if(!empty($errors -> errorText)) {
                $response -> error = $errors;
                $response -> status = 0;
            }else{
                $response -> error = null;
                $response -> status = 1;
            }

            $student = new stdClass();
            $student -> group = $group;
            $student -> name = $firstName." ".$lastName;
            include 'constants.php';
            $student -> gender = searchArrayByValue($genders, $gender);
            $student -> birthday = $birthday;
            $response -> student = $student;


        header('Content-Type: application/json');

        echo json_encode($response);
        exit;
    } elseif ($_POST["type"] == "delete") {
        $response = array('status' => 'success');
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }
}  
function searchArrayByValue($array, $value) {
    foreach ($array as $key => $val) {
        if ($val == $value) {
            return $key;
        }
    }
    return null;}
