const assets = [
    '/',
    '/index.php',
    '/styles/styles.css',
    '/scripts/lab1.js',
    'photos/myprofile.jpg', 
    'photos/bell.png',
    'photos/maxresdefault.jpg',
    'photos/bell_2.jpg', 
    'photos/bell_3.jpg',
    'photos/Icon-72.png'
  ]

self.addEventListener("install", installEvent => {
    installEvent.waitUntil(
    caches.open("pwa-assets").then(cache => {
        cache.addAll(assets)
    })
    )
})

/*self.addEventListener("fetch", fetchEvent => {
    fetchEvent.respondWith(
      caches.match(fetchEvent.request).then(res => {
        return res || fetch(fetchEvent.request)
      })
    )
  })*/
