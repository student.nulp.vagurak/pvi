let rowToEdit;
let rowToDelete;
let idOfCurrentUser = 0;
const genders = new Map([
  ['Male', 1],
  ['Female', 2],
  ['Povtorka', 3],
]);

const btnAddStudentForm = document.querySelector(".btn--form-submit");
const form = document.querySelector(".student-form");
const studentDeleteWindow = document.querySelector(".delete-student-window");
const studentWindow = document.querySelector(".student-window");
const btnCloseStudentWindow1 = studentWindow.querySelector(".btn--close-student-window");
const btnCloseStudentWindow2 = studentWindow.querySelector(".btn--form-cancel");
const btnDeleteStudentSubmit = studentDeleteWindow.querySelector(".btn--delete-student-submit");
const btnDeleteStudentCancel1 = studentDeleteWindow.querySelector(".btn--delete-student-cancel");
const btnDeleteStudentCancel2 = studentDeleteWindow.querySelector(".btn--close-student-window");
document.querySelector("#birthday").max = new Date().toISOString().split("T")[0];

if ("serviceWorker" in navigator) {
    self.addEventListener("load", async () => {
        const container = navigator.serviceWorker;
        if (container.controller === null) {
            const reg = await container.register("sw.js");
        }
    });
  }
  



const btnAddStudent = document.querySelector("#buttonAddStudent");


function openAddStudentWindow() {
    studentWindow.classList.add("opened");
    const studentWindowHeading = studentWindow.querySelector("h3");
    studentWindowHeading.innerHTML = "Add student";
    form.querySelectorAll("input").forEach((input) => (input.value = ""));
    form.querySelectorAll("select").forEach((select) => (select.value = ""));
  }
  
  btnAddStudent.addEventListener("click", openAddStudentWindow);
  
  
  const tableContent = document.querySelector(".table-content");  

function addStudent(e) {

    let isFormValid = form.checkValidity();
    if (!isFormValid) {
      form.reportValidity();
    } else {
      e.preventDefault(); //для демонстрації запиту потрібно закоментити
      form.checkValidity();
      let group, firstName, gender, birthday, lastName;
      group = form.querySelector('[name="group"]').value;
      firstName = form.querySelector('[name="first-name"]').value;
      lastName = form.querySelector('[name="last-name"]').value;
      gender = form.querySelector('[name="gender"]').value;
      birthday = form.querySelector('[name="birthday"]').value;
      let xhr = new XMLHttpRequest();
      let url = "server.php";
      let data = new FormData();
      data.append("type", "addOrEdit");
      data.append("group", group);
      data.append("firstName", firstName);
      data.append("lastName", lastName);
      data.append("gender", gender);
      data.append("birthday", birthday);
      try {
          data.append("id", rowToEdit.id);
      }catch (err){
          data.append("id", -1);
      }
      xhr.open("POST", url, true);
      xhr.onload = function () {
          if (xhr.status === 200) {
              let response = JSON.parse(xhr.responseText);
              if (response.status === 1) {
                  let student = response.student;
                  if (studentWindow.querySelector("h3").textContent === "Add student") {
                      let table = document.getElementById("table");
                      let row = table.insertRow(table.length);
                      row.id = idOfCurrentUser;
                      let cell1 = row.insertCell(0);
                      cell1.classList.add("col");
                      let cell2 = row.insertCell(1);
                      cell2.classList.add("col");
                      let cell3 = row.insertCell(2);
                      cell3.classList.add("col");
                      let cell4 = row.insertCell(3);
                      cell4.classList.add("col");
                      let cell5 = row.insertCell(4);
                      cell5.classList.add("col");
                      let cell6 = row.insertCell(5);
                      cell6.classList.add("col");
                      let cell7 = row.insertCell(6);
                      cell7.classList.add("col");
                      let span = document.createElement('span');
                      let checkbox = document.createElement('input');
                      span.appendChild(checkbox);
                      checkbox.type = 'checkbox';
                      checkbox.addEventListener("click", () => {
                          changeCircleColor(cell6)
                      });
                      cell1.appendChild(span);
                      cell2.innerHTML = "<span>" + student.group + "</span>"
                      cell3.innerHTML = "<span>" + student.name + "</span>"
                      cell4.innerHTML = "<span>" + student.gender + "</span>"
                      cell5.innerHTML = "<span>" + student.birthday + "</span>"
                      cell6.innerHTML = '<span class="circle d-inline-block"></span>';
                      let spanContainer = document.createElement("span");
                      spanContainer.classList.add("buttonContainer", "d-flex", "justify-content-center");
                      cell7.appendChild(spanContainer);
                      let buttonEdit = document.createElement("button");
                      buttonEdit.classList.add("tableButton", "p-0");
                      spanContainer.appendChild(buttonEdit);
                      let penIcon = document.createElement("i");
                      penIcon.classList.add("fa-solid", "fa-pen");
                      buttonEdit.appendChild(penIcon);
                      let removeButton = document.createElement("button");
                      removeButton.classList.add("tableButton", "p-0");
                      spanContainer.appendChild(removeButton);
                      let removeIcon = document.createElement("i");
                      removeIcon.classList.add("fa-solid", "fa-xmark");
                      removeButton.appendChild(removeIcon);
                      removeButton.addEventListener("click", () => {
                          openDeleteStudentWindow(row)
                      });
                      buttonEdit.addEventListener("click", () => {
                          openEditStudentWindow(row)
                      });
                      idOfCurrentUser++;
                  } else {
                      const tds = rowToEdit.querySelectorAll("td");
                      rowToEdit.id = -1;
                      tds[1].innerHTML = student.group;
                      tds[2].innerHTML = student.name;
                      tds[3].innerHTML = student.gender;
                      tds[4].innerHTML = student.birthday;
                  }
                  studentWindow.classList.remove("opened");
              }else {
                  switch (response.error.errorText){
                      case 'Empty group':
                          let groupErrorElement = document.querySelector(".group-error");
                          groupErrorElement.classList.add("opened");
                          document.querySelector("#group").addEventListener("click", () => {
                              groupErrorElement.classList.remove("opened");
                          })
                          break;
                      case 'Empty first name':
                          let firstNameErrorElement = document.querySelector(".first-name-error");
                          firstNameErrorElement.classList.add("opened");
                          document.querySelector("#first-name").addEventListener("click", () => {
                              firstNameErrorElement.classList.remove("opened");
                          })
                          break;
                      case 'Empty last name':
                          let lastNameErrorElement = document.querySelector(".last-name-error");
                          lastNameErrorElement.classList.add("opened");
                          document.querySelector("#last-name").addEventListener("click", () => {
                              lastNameErrorElement.classList.remove("opened");
                          })
                          break;
                      case 'Wrong gender value':
                      case 'Empty gender':
                          let genderErrorElement = document.querySelector(".gender-error");
                          genderErrorElement.classList.add("opened");
                          document.querySelector("#gender").addEventListener("click", () => {
                              genderErrorElement.classList.remove("opened");
                          })
                          break;
                      case 'Empty birthday':
                          let birthdayErrorElement = document.querySelector(".birthday-error");
                          birthdayErrorElement.classList.add("opened");
                          document.querySelector("#birthday").addEventListener("click", () => {
                              birthdayErrorElement.classList.remove("opened");
                          })
                          break;
                  }

                }}else {
                alert("Error: " + xhr.statusText);
                }
        
              };
              xhr.send(data);
          }
      
}

function openEditStudentWindow(row) {
    studentWindow.classList.add("opened");
    const studentWindowHeading = studentWindow.querySelector("h3");
    studentWindowHeading.innerHTML = "Edit student";
  
    rowToEdit = row;
    const tds = rowToEdit.querySelectorAll("td");

  
    console.log(currentRow);
  
    const inputs = form.querySelectorAll("input");
  
    console.log(inputs);
  
    inputs[0].value = tds[2].innerText.split(" ")[0];
    inputs[1].value = tds[2].innerText.split(" ")[1];
    inputs[2].value = tds[4].innerText;
  
    const selects = form.querySelectorAll("select");
  
    selects[0].value = tds[1].innerText;
    selects[1].value = genders.get(tds[3].innerText);
  
  
  }
  
function closeStudentWindow() {
    studentWindow.classList.remove("opened");
  }
  
  
    
  btnCloseStudentWindow1.addEventListener("click", closeStudentWindow);
  btnCloseStudentWindow2.addEventListener("click", closeStudentWindow);
  
btnAddStudentForm.addEventListener("click", addStudent);



function openDeleteStudentWindow(e) {
  studentDeleteWindow.classList.add("opened");
  rowToDelete = e;
  const name = studentDeleteWindow.querySelector(".student-to-delete");
  const tds = rowToDelete.querySelectorAll("td");
  name.innerText = tds[2].innerText;
 
}
if ("serviceWorker" in navigator) {
    self.addEventListener("load", async () => {
        const container = navigator.serviceWorker;
        if (container.controller === null) {
            const reg = await container.register("sw.js");
        }
    });
  }
  

function removeStudent(el) {
    if(confirm("Are you sure you want to delete user?"))
        el.remove();
}
function changeCircleColor(tableCell){
    let spanCircle = tableCell.querySelectorAll('.circle').item(0);
    if(spanCircle.classList.contains('active')){
        spanCircle.classList.remove('active')
    }else{
        spanCircle.classList.add('active')
    }
}

function initListeners(){
    document.getElementById("buttonAddStudent").addEventListener("click", addStudent)
}

  
function closeDeleteWindow(e) {
    studentDeleteWindow.classList.remove("opened");
  }
  
  function deleteStudentSubmit(e) {
    e.preventDefault();
    let xhr = new XMLHttpRequest();
    let url = "server.php";
    let data = new FormData();
    data.append("type", "delete");
    data.append("id", rowToDelete.id);
    xhr.open("POST", url, true);
    xhr.onload = function () {
        if (xhr.status === 200) {
            let response = JSON.parse(xhr.responseText);
            if (response.status === "success") {
                rowToDelete.remove();
            } else {
                alert(response.error.errorText);
            }
        }
    };
    xhr.send(data);

    studentDeleteWindow.classList.remove("opened");
  }
  
  btnDeleteStudentSubmit.addEventListener("click", deleteStudentSubmit);
  btnDeleteStudentCancel1.addEventListener("click", closeDeleteWindow);
  btnDeleteStudentCancel2.addEventListener("click", closeDeleteWindow);
  
